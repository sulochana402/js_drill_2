// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

function getCorrectedSalaries(data) {
  if (Array.isArray(data)) {
    let allSalaries = [];
    for (let index = 0; index < data.length; index++) {
      let parsedSalaries = parseFloat(data[index].salary.replace("$", ""));
      //   console.log(parsedSalaries);
      let correctedSalaries = parsedSalaries * 10000;
      allSalaries.push(correctedSalaries);
    }
    return allSalaries;
  } else {
    return [];
  }
}
module.exports = getCorrectedSalaries;
