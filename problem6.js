// 6. Find the average salary of based on country. ( Groupd it based on country and then find the average ).

function calculateAverageSalariesByCountry(data) {
  if (Array.isArray(data)) {
    const totalSalariesByCountry = [];
    const countByCountry = [];
    const averageSalariesByCountry = [];

    for (let index = 0; index < data.length; index++) {
      const person = data[index];
      let parsedSalaries = parseFloat(person.salary.replace("$", ""));
      if (!totalSalariesByCountry[person.location]) {
        totalSalariesByCountry[person.location] = 0;
        countByCountry[person.location] = 0;
      }
      totalSalariesByCountry[person.location] += parsedSalaries;
      countByCountry[person.location]++;
    }

    // Calculate average salaries for each country
    for (const country in totalSalariesByCountry) {
      averageSalariesByCountry[country] =
        totalSalariesByCountry[country] / countByCountry[country];
    }
    return averageSalariesByCountry;
  } else {
    return [];
  }
}
module.exports = calculateAverageSalariesByCountry;
