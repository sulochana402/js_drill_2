// 5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).

function calculateTotalSalariesByCountry(data) {
  if (Array.isArray(data)) {
    const salariesByCountry = [];
    for (let index = 0; index < data.length; index++) {
      const person = data[index];
      let parsedSalaries = parseFloat(data[index].salary.replace("$", ""));
      if (!salariesByCountry[person.location]) {
        salariesByCountry[person.location] = 0;
      }
      salariesByCountry[person.location] += parsedSalaries;
    }
    return salariesByCountry;
  }
}
module.exports = calculateTotalSalariesByCountry;
