// 2. Convert all the salary values into proper numbers instead of strings.

function convertSalariesToNumbers(data) {
  if (Array.isArray(data)) {
    let properNumbers = [];
    for (let index = 0; index < data.length; index++) {
      let values = parseFloat(data[index].salary.replace("$", ""));
      properNumbers.push(values);
    }
    return properNumbers;
  } else {
    return [];
  }
}
module.exports = convertSalariesToNumbers;
