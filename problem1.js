// 1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )

function findAllWebDevelopers(data) {
  if (Array.isArray(data)) {
    let webDevelopers = [];
    for (let index = 0; index < data.length; index++) {
      if (data[index].job.toLowerCase().includes("web developer")) {
        webDevelopers.push(data[index]);
      }
    }
    return webDevelopers;
  } else {
    return [];
  }
}
module.exports = findAllWebDevelopers;
