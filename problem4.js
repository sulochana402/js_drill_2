// 4. Find the sum of all salaries.

function calculateTotalSlaries(data) {
  if (Array.isArray(data)) {
    let sum = 0;
    for (let index = 0; index < data.length; index++) {
      let parsedSalaries = parseFloat(data[index].salary.replace("$", ""));
      sum += parsedSalaries;
    }
    return sum;
  } else {
    return [];
  }
}
module.exports = calculateTotalSlaries;
